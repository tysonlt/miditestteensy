#include <MIDI.h>
#include <QueueList.h>
#include <Timer.h>

#include "State.h"
#include "Screen.h"

#define DEBUG

#define DEFAULT_REFRESH_INTERVAL  2000
#define TAP_TEMPO_LED_PIN         6
#define TAP_TEMPO_LED_TIMEOUT     80

void dumpHex(byte *arr, int len, int from = 0, int to = 0);

Timer timer;

//NOTE: REQUIRES a platform with multiple serials (ie, not Nano)
MIDI_CREATE_INSTANCE(HardwareSerial, Serial1, MIDI);

/**
 * 
 */
void setup() {

  pinMode(TAP_TEMPO_LED_PIN, OUTPUT);
  
  Serial.begin(9600);  
  Serial.println("------------------------------------");
  Serial.println("AXE FX III MIDI TEST - Teensy ++ 2.0");
  Serial.println("------------------------------------");
  Serial.println();

  screen.init();
  
  MIDI.begin();
  MIDI.turnThruOff();
  MIDI.setHandleProgramChange(onProgramChange);
  MIDI.setHandleSystemExclusive(onSystemExclusive);
  state.channel = 1; //TODO: read from Axe?

  timer.every(DEFAULT_REFRESH_INTERVAL, refresh);

}

/**
 * 
 */
void loop() {

  //update the timer
  timer.update();
    
  //make sure we have the firmware version and current patch after power-on
  if (!state.firmwareRequested) {
    requestFirmwareVersion();
    refresh();
    state.firmwareRequested = true;
  }

  //block for MIDI read
  while (!MIDI.read()) {
    //update the timer again or it gets stuck
    timer.update();
  }

  //debugs
  #ifdef DEBUG
  printAll();
  #endif

  //update screen
  screen.update();
  
}

/**
 * Request current patch and tempo data.
 */
void refresh() {
  requestPresetDetails();
}

/**
 * 
 */
void printAll() {

  //CHECKS DIRTY BUT DOESN'T CLEAN!!!
  
  char buf[100];

  if (state.isDirty(DIRTY_FIRMWARE)) {
    snprintf(buf, sizeof(buf), "AXE FX III %d.%02d (USB %d.%02d)", 
      state.firmwareMajor,
      state.firmwareMinor,
      state.usbMajor,
      state.usbMinor
    );
    Serial.println(buf);
  }

  if (state.isDirty(DIRTY_PRESET)) {
    snprintf(buf, sizeof(buf), "PRESET: [%d] %s", state.preset, state.presetName);
    Serial.println(buf);
  }

  if (state.isDirty(DIRTY_SCENE)) {
    snprintf(buf, sizeof(buf), "SCENE : [%d] %s", state.scene+1, state.sceneName);
    Serial.println(buf);
  }

  if (state.isDirty(DIRTY_TEMPO)) {
    snprintf(buf, sizeof(buf), "TEMPO : %d", state.tempo);
    Serial.println(buf);
  }
  
}

/**
 * Blink the on-board led.
 */
void tapTempoNotify() {
  digitalWrite(TAP_TEMPO_LED_PIN, HIGH);
  timer.after(TAP_TEMPO_LED_TIMEOUT, tapTempoLedOff);
}

/**
 * Damn, the led on the Teensy++ 2.0 is bright!!!
 */
void tapTempoLedOff() {
  digitalWrite(TAP_TEMPO_LED_PIN, LOW);
}
