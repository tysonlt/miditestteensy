
/**
 * 
 */
void updatePresetNumber(byte bank, byte patch) {
  state.bank = bank;
  state.patch = patch;
  state.preset = buildPresetNumber(bank, patch);
}

/**
 * 
 */
int buildPresetNumber(byte bank, byte patch) {
  return (bank * BANK_SIZE) + patch;
}

/**
 * 
 */
int buildTempo(byte lsb, byte msb) {
  return lsb + (msb * 128);
}

/**
 * 
 */
void updateSceneNumber(byte scene) {
  state.scene = scene;
}

/**
 * 
 */
boolean isAxeSysEx(const byte *sysex, const int len) {
  return 
    len > 4 && 
    sysex[1] == AXE_MANUFACTURER_B1 && 
    sysex[2] == AXE_MANUFACTURER_B2 && 
    sysex[3] == AXE_MANUFACTURER_B3;
}
    
/**
 * Returns whether there were enough bytes to parse.
 */
boolean parseName(const byte *sysex, byte length, byte offset, char *buffer, byte size) {
  memset(buffer, ' ', size);
  if (length > offset+1) { //header plus checksum
    for (byte i=offset; i<size+offset; i++) {
      buffer[i-offset] = (char) sysex[i];
    }
    buffer[size] = '\0';
    return true;
  }
  return false;
}

/**
 * 
 */
void dumpHex(byte *arr, int len, int from, int to) {
  char buf[6];
  if (to == 0) to = len;
  for (byte i=from; i<to; i++) {
    snprintf(buf, sizeof(buf), "0x%02X ", arr[i]);
    Serial.print(buf);
  }
  Serial.println();
}
