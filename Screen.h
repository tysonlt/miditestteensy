#pragma once

#include <Adafruit_ST7735.h>
#include "State.h"

//SPI
#define SCREEN_CS_PIN           10
#define SCREEN_RS_PIN           9
#define SCREEN_TYPE             INITR_BLACKTAB

//dimensions of printable areas
#define SCREEN_OFFSET           0
#define SCREEN_W                160
#define SCREEN_H                128
#define SCREEN_ROTATION         1
#define CHAR_W                  6
#define CHAR_H                  8
#define MAX_CHAR_PER_LINE       26 //at font size 1, NOT including null byte

//Font
typedef byte FontSize;
#define FONT_SIZE_1             1
#define FONT_SIZE_2             2
#define FONT_SIZE_3             3
#define FONT_SIZE_4             4
#define FONT_SIZE_5             5
#define FONT_SIZE_6             6
#define FONT_SIZE_7             7

struct Point {
  byte x;
  byte y;
};

//positions to write various things at
struct {
  //byte arithmetic results in int, so coerce operations back to byte
  const Point PRESET_NUMBER     = { SCREEN_OFFSET, SCREEN_OFFSET };
  const Point PRESET_TITLE      = { SCREEN_OFFSET, (byte) ((CHAR_H * FONT_SIZE_4) + 13) };
  const Point SCENE_NUMBER      = { SCREEN_OFFSET, (byte) (PRESET_TITLE.y + (CHAR_H * FONT_SIZE_2 * 2) + 5) };
  const Point SCENE_TITLE       = { (byte) ((CHAR_W * FONT_SIZE_4) + 5), SCENE_NUMBER.y };
  const Point FOOTER            = { SCREEN_OFFSET, SCREEN_H - CHAR_H };
  const Point BOOT_SPLASH       = { SCREEN_OFFSET, SCREEN_OFFSET };
} POS;

typedef uint16_t Colour;

//Basic colours
#define BLACK                   0x0000
#define BLUE                    0x001F
#define RED                     0xF800
#define GREEN                   0x07E0
#define CYAN                    0x07FF
#define MAGENTA                 0xF81F
#define YELLOW                  0xFFE0
#define WHITE                   0xFFFF

//System colours 
#define COLOUR_BACKGROUND       BLACK
#define COLOUR_PRESET_NUM       RED
#define COLOUR_TITLE            CYAN
#define COLOUR_SCENE            YELLOW
#define COLOUR_FOOTER           MAGENTA
#define COLOUR_ERROR            RED

/**
 * 
 */
class Screen {

  public:
  
    /**
     * 
     */
    void init() {
      
      tft_.initR(SCREEN_TYPE);  
      tft_.setRotation(SCREEN_ROTATION);
      
      drawBootScreen();
      
    }

    /**
     * 
     */
    void update() {

      if (state.popDirty(DIRTY_PRESET)) {
        drawPresetDetails();
      }

      if (state.popDirty(DIRTY_SCENE)) {
        drawSceneDetails();
      }

      if (state.popDirty(DIRTY_TEMPO)) {
        drawFooter();
      }

      //we're not drawing anything else, so clear it
      state.cleanAll();
       
    }

    /**
     * To show while connecting to Axe.
     */
    void drawBootScreen() {

      char buf[20];
      static byte count = 0;

      if (count == 0) {
        clearScreen();
      }

      snprintf(buf, sizeof(buf), "Connecting: [%d]", count++);
      printText(buf, POS.BOOT_SPLASH, FONT_SIZE_1, YELLOW);
      
    }
    
    /**
     * Prints buffer at the specified point.
     * 
     * Inverted colours will result in a rect drawn in the specified colour, and text drawn in background colour.
     */
    void printText(
      const char *text, 
      const Point p = {0, 0}, 
      const FontSize textSize = FONT_SIZE_1, 
      const Colour colour = COLOUR_TITLE, 
      const boolean invertColours = false) {    

      //offset the text if in inverted mode
      byte nudge = 0;            
      if (invertColours) {
        
        //we need to draw a rect because the char is drawn too close to the edge
        nudge = 2; //push the text further into the box, rather than the hard edge
        tft_.setTextColor(COLOUR_BACKGROUND);  
        tft_.fillRect(p.x, p.y, CHAR_W * textSize, CHAR_H * textSize, colour);
        
      } else {

        //regular draw with flicker-free transparent background
        tft_.setTextColor(colour, COLOUR_BACKGROUND);  
        
      }           

      //set coords and text behaviour
      tft_.setCursor(p.x + nudge, p.y + nudge);
      tft_.setTextSize(textSize);
      tft_.setTextWrap(false);

      //punch it!
      tft_.print(text);

    }

    /**
     * Wrap text over two lines.
     * 
     * Will not print more than two lines.
     * 
     * NOTE: can't use TFT.setTextWrap, because we don't always want wrap to start at x=0.
     */
    void printWrappedText(
      const char *text, 
      const Point p, 
      const byte lineMax = MAX_CHAR_PER_LINE, 
      const byte fontSize = FONT_SIZE_1, 
      const Colour colour = COLOUR_TITLE) {

      char buf[lineMax + 1];
      
      //print the first line of chars
      snprintf(buf, lineMax + 1, text);
      printText(buf, p, fontSize, colour);

      //print second line, truncating at lineMax*2 chars (just have to miss the rest if text is longer than 2 lines)
      Point p2 = {p.x, (byte) (p.y + CHAR_H * fontSize)};  
      snprintf(buf, lineMax + 1, strlen(text) > lineMax ? text + lineMax : "");
      printText(buf, p2, fontSize, colour);
      
    }


    /**
     * TODO
     */
    void drawFullScreenTuner(const char *note, const byte finetune) {

      char buf[5];    
      int pos = finetune - 63;

      snprintf(buf, 4, "%d", pos);      
      if (finetune == 63) {
        //we are in tune
      }

      printText(buf, {0, 0}, SCREEN_W, FONT_SIZE_7);
      
    }
    
    /**
     * 
     */
    void clearScreen(Colour colour = COLOUR_BACKGROUND) {
      tft_.fillScreen(colour);
    }
    
  private:

    /**
     * 
     */
    void drawPresetDetails() {

      size_t size = 50;
      char buf[size];
      
      //print the number
      snprintf(buf, 4, "%03d", state.preset);
      printText(buf, POS.PRESET_NUMBER, FONT_SIZE_5, COLOUR_PRESET_NUM);

      //put the title into a space-filled buffer to erase previous title
      memset(buf, ' ', size);
      
      //print title
      printWrappedText(
        state.presetName, 
        POS.PRESET_TITLE, 
        MAX_CHAR_PER_LINE / FONT_SIZE_2, 
        FONT_SIZE_2, COLOUR_TITLE
      );
      
    }

    /**
     * 
     */
    void drawSceneDetails() {

      size_t size = 50;
      char buf[size];
      
      //scene num
      snprintf(buf, 2, "%d", state.scene + 1);
      printText(buf, POS.SCENE_NUMBER, FONT_SIZE_4, COLOUR_SCENE, true);

      //put the title into a space-filled buffer to erase previous title
      memset(buf, ' ', size);
      
      //print title
      printWrappedText(
        state.sceneName, 
        POS.SCENE_TITLE,
        (MAX_CHAR_PER_LINE / FONT_SIZE_2) - (FONT_SIZE_4 / FONT_SIZE_2), 
        FONT_SIZE_2, COLOUR_SCENE
      );
      
    }

    /**
     * 
     */
    void drawFooter() {
      size_t size = 50;
      char buf[size];
      snprintf(buf, MAX_CHAR_PER_LINE + 1, "TEMPO: %d BPM   ", state.tempo);
      printText(buf, POS.FOOTER, FONT_SIZE_1, COLOUR_FOOTER);
    }

    Adafruit_ST7735 tft_ = Adafruit_ST7735(SCREEN_CS_PIN, SCREEN_RS_PIN, -1);   
      
} screen;
