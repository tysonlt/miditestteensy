#pragma once

#define MIDI_CHANNEL                  1
#define MIDI_BANK_CC                  0
#define BANK_SIZE                     128
#define MAX_PRESET_NAME               32
#define MAX_SCENE_NAME                MAX_PRESET_NAME

#define AXE_MANUFACTURER_B1           0x00
#define AXE_MANUFACTURER_B2           0x01
#define AXE_MANUFACTURER_B3           0x74
#define AXE_QUERY_BYTE                0x7F
#define AXE_CHECKSUM_PLACEHOLDER      0x00

#define SYSEX_HEADER                  0x00, 0x01, 0x74, 0x10
#define SYSEX_REQUEST_FIRMWARE        0x08
#define SYSEX_REQUEST_PRESET_INFO     0x0D
#define SYSEX_REQUEST_SCENE_INFO      0x0E
#define SYSEX_REQUEST_SCENE_NUMBER    0x0C
#define SYSEX_TAP_TEMPO               0x10
#define SYSEX_REQUEST_TEMPO           0x14

const byte DIRTY_PRESET    = 1<<0;
const byte DIRTY_SCENE     = 1<<1;
const byte DIRTY_TEMPO     = 1<<2;
const byte DIRTY_FIRMWARE  = 1<<3;

struct {
  byte channel;
  byte bank;
  byte patch;
  byte scene;
  int preset;
  bool firmwareRequested;
  char sceneName[MAX_SCENE_NAME+1], presetName[MAX_PRESET_NAME+1];
  int tempo = 0;
  byte firmwareMajor;
  byte firmwareMinor;
  byte usbMajor;
  byte usbMinor;

  byte dirty = 0;

  void setDirty(byte flag) {
    dirty |= flag;
  }

  void setClean(byte flag) {
    dirty &= ~flag;
  }

  boolean popDirty(byte flag) {
    bool result = isDirty(flag);
    setClean(flag);
    return result;
  }

  void cleanAll() {
    dirty = 0;
  }

  boolean isDirty(byte flag) {
    return dirty & flag;
  }
  
} state;
