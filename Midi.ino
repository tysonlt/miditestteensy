//TODO: put this in a class
#include "State.h"

/**
 * 
 */
void onProgramChange(byte channel, byte number) {
  
  #ifdef DEBUG
  Serial.print("PROGRAM CHANGE: ");
  Serial.println(number);
  #endif
  
  requestPresetDetails();
}

/**
 * 
 */
void onSystemExclusive(byte *sysex, unsigned length) {
  processAxeSysEx(sysex, length);
}

/**
 * Process a single sysex message from the Axe.
 * 
 * TODO: occasionally getting corrupt version of scene/preset name, truncated with funny chars
 * TODO: empty scene name not overwriting previous
 */
void processAxeSysEx(byte *sysex, unsigned length) {
  
  switch (sysex[5]) {

    case SYSEX_TAP_TEMPO:
      tapTempoNotify();
      break;

    case SYSEX_REQUEST_FIRMWARE: 
      state.firmwareMajor = sysex[6];
      state.firmwareMinor = sysex[7];
      state.usbMajor = sysex[9];
      state.usbMinor = sysex[10];
      //just set dirty, this is not often called (and not printed at the moment)
      state.setDirty(DIRTY_FIRMWARE);
      break;

    case SYSEX_REQUEST_PRESET_INFO:
      char tempPreset[MAX_PRESET_NAME+1];
      if (parseName(sysex, length, 8, tempPreset, MAX_PRESET_NAME)) {
        if (0 != strcmp(tempPreset, state.presetName) || buildPresetNumber(sysex[7], sysex[6]) != state.preset) {
          state.setDirty(DIRTY_PRESET);
        }
        updatePresetNumber(sysex[7], sysex[6]);
        strcpy(state.presetName, tempPreset);
      }
      break;

    case SYSEX_REQUEST_SCENE_INFO:
      char tempScene[MAX_SCENE_NAME+1];
      if (parseName(sysex, length, 7, tempScene, MAX_SCENE_NAME)) {
        if (0 != strcmp(tempScene, state.sceneName) || sysex[6] != state.scene) {
          state.setDirty(DIRTY_SCENE);
        }
        updateSceneNumber(sysex[6]);
        strcpy(state.sceneName, tempScene);
      }
      break;

    case SYSEX_REQUEST_SCENE_NUMBER:
      if (sysex[6] != state.scene) {
        state.setDirty(DIRTY_SCENE);
      }
      break;

    case SYSEX_REQUEST_TEMPO:
      int tempo = buildTempo(sysex[6], sysex[7]);
      if (tempo != state.tempo) {
        state.tempo = tempo;
        state.setDirty(DIRTY_TEMPO);
      }
      break;

  }; //end switch
  
}

/**
 * Convert int preset into bank/patch and send.
 * 
 * Axe will send a sysex on preset change, which prompts us
 * to read the new preset/scene name.
 * 
 * NOTE: Assumes 0-based preset. Maybe ask Axe about preset num offset param.
 */
void sendPresetChange(const int preset) {

  //calculate bank/patch
  byte bank = preset / BANK_SIZE;
  byte patch = preset - (BANK_SIZE * bank);

  //send bank change
  MIDI.sendControlChange(MIDI_BANK_CC, bank, state.channel);

  //send patch change
  MIDI.sendProgramChange(patch, state.channel);

}

/**
 * Get all preset info.
 */
void requestPresetDetails() {
  requestPresetName();
  requestSceneName();
  requestSceneNumber(); //needed to set dirty if scene name is blank
  requestTempo();
}

/**
 * 
 */
void requestFirmwareVersion() {
  const byte cmd[] = {
    SYSEX_HEADER, 
    SYSEX_REQUEST_FIRMWARE, 
    AXE_CHECKSUM_PLACEHOLDER
  };
  sendSysEx(6, (byte*) cmd);
}

/**
 * 
 */
void requestPresetName() {
  const byte cmd[] = {
    SYSEX_HEADER, 
    SYSEX_REQUEST_PRESET_INFO, 
    AXE_QUERY_BYTE, 
    AXE_QUERY_BYTE, 
    AXE_CHECKSUM_PLACEHOLDER
  };
  sendSysEx(8, (byte*) cmd);
}

/**
 * 
 */
void requestTempo() {
  const byte cmd[] = {
    SYSEX_HEADER, 
    SYSEX_REQUEST_TEMPO, 
    AXE_QUERY_BYTE, 
    AXE_QUERY_BYTE, 
    AXE_CHECKSUM_PLACEHOLDER
  };
  sendSysEx(8, (byte*) cmd);
}

/**
 * 
 */
void requestSceneNumber() {
  const byte cmd[] = {
    SYSEX_HEADER, 
    SYSEX_REQUEST_SCENE_NUMBER, 
    AXE_QUERY_BYTE, 
    AXE_CHECKSUM_PLACEHOLDER
  };
  sendSysEx(7, (byte*) cmd);
}

/**
 * 
 */
void requestSceneName() {
  const byte cmd[] = {
    SYSEX_HEADER, 
    SYSEX_REQUEST_SCENE_INFO, 
    AXE_QUERY_BYTE, 
    AXE_CHECKSUM_PLACEHOLDER
  };
  sendSysEx(7, (byte*) cmd);
}

/**
 * Calculate checksum and send.
 */
void sendSysEx(const byte length, byte *sysex) {

  byte sum = 0xF0;
  for (int i=0; i<length-1; ++i) {
    sum ^= sysex[i];
  }
  sysex[length-1] = (sum & 0x7F);

  MIDI.sendSysEx(length, sysex);
  
}
